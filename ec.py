from functools import wraps
import requests

def sendlog(func): 
	'''
        Decorator that send logs.
	'''

	@wraps(func)
	def wrapper(*args, **kwargs):
		try:
			result = func(*args, **kwargs) 
			return result
		except Exception,e:
			print 'e:', e
			url = 'http://localhost:8888/info'
			payload = {'Error': str(e)}
			r = requests.post(url, data=payload)
	return wrapper