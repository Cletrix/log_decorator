import tornado.ioloop
import tornado.web
from tornado import autoreload
import tornado.options
import tornado.httpserver
import json
import signal
import os


class Control:
    def __init__(self):
        self.logs = []

    def create_user(self, info):
        self.logs.append(info)
        print 'users:', self.logs


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

    def post(self):

        req =  str(self.request.body)
        control.create_user(req)

        self.write("Hello, world")

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/info", MainHandler),
        ]
        settings = dict(
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            xsrf_cookies=False,
            autoreload=True,
            debug=True
        )
        tornado.web.Application.__init__(self, handlers, **settings)

def main():
    tornado.options.parse_command_line()
    app = Application()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

def signal_handler(signal, frame):
    print 'You pressed Ctrl+C! - Bye'
    os._exit(0)

if __name__ == "__main__":
    control = Control()
    signal.signal(signal.SIGINT, signal_handler)
    main()
    sys.exit(0)
